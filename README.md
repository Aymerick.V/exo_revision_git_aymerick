> Cet exercice est une revision qui vous permettra de vous familiariser avec les concepts de base de Git et de GitLab, tels que la création de dépôts, les branches, les commits et les pushs. Ils pourront ainsi comprendre comment travailler en équipe et gérer les versions de leur code de manière efficace.

- Créez un nouveau projet sur GitLab en lui donnant un nom approprié, par exemple "Exo_Revision-Git_nom".
- Dans votre environnement de développement local, initialisez un nouveau dépôt Git dans un répertoire vide.
- Créez un fichier nommé index.html et un autre style.css dans le répertoire du projet et ajoutez-y quelques lignes de code fourni.
- Ajoutez le fichier à l'index Git.
- Effectuez un commit initial avec un message descriptif.
- Ajoutez le dépôt GitLab en tant que dépôt distant. Vous pouvez obtenir l'URL du dépôt GitLab en accédant à la page de votre projet sur GitLab.
- Poussez votre branche principale (généralement appelée main) vers le dépôt distant
- Créez une nouvelle branche appelée new_Function
- Passez à la nouvelle branche
- Modifiez le fichier index.html pour ajouter une nouvelle fonctionnalité.
- Ajoutez et effectuez un commit des modifications
- Poussez la branche new_Function vers le dépôt distant
- Revenez à la branche principale
- Fusionnez la branche new_Function dans la branche principale 
- Poussez les modifications fusionnées vers le dépôt distant